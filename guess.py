from sys import argv

def guess(x=42, y=1, z=100):
    g = z//2
    mini=y
    maxi=z
    t = 0
    while g != x:
        print("Try:", t)
        print("Guess:", g, end=" - ")
        if g < x:
            print("more")
            mini = g+1
        else:
            print("less")
            maxi = g-1
        g = (mini+maxi)//2
        t+=1
    return f"Correct! It's: {g}"

if __name__ == "__main__":
    try:
        s = int(argv[1])
        st = int(argv[2])
        c = int(argv[3])
    except:
        s = int(input("Lower range of numbers to guess: "))
        st = int(input("Higher range of numbers to guess: "))
        c = int(input("Number to guess: "))
    if c >= s and c <= st:
        print(guess(c, s, st))
    else:
        print(f"Number to guess have to be between {s} and {st}")
