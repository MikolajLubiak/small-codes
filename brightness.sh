#!/usr/bin/env ksh
xbacklight -set $1
xrandr --output LVDS-1 --brightness $(echo "scale=2; $1/100" | bc)
doas wsconsctl display.brightness=$1
