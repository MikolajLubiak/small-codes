# $OpenBSD: dot.profile,v 1.7 2020/01/24 02:09:51 okan Exp $
#
# sh/ksh initialization

PATH=$HOME/bin:/bin:/sbin:/usr/bin:/usr/sbin:/usr/X11R6/bin:/usr/local/bin:/usr/local/sbin:/usr/games:$HOME/.cargo/bin
export PATH HOME TERM

alias ls="/bin/ls -la "
alias l="/bin/ls -la "
alias d="/usr/bin/doas "
alias e="/usr/local/bin/nvim "
alias rm="/bin/rm -rf "
alias mkdir="/bin/mkdir -p "
alias pip="python3 -m pip "
alias gh="$HOME/prog/sh/git-script/git.sh "
alias sn="$HOME/prog/sh/send-script/send.sh "
alias s="shutdown -ph now"
alias so="xset dpms force off"
